class Article < ActiveRecord::Base
    validates :title, presence: true, length: { minimum: 5 }
    validates :slug, uniqueness: true, presence: true, exclusion: {in: %w[signout auth]}
    validates :category_id, presence: true

    before_validation :generate_slug
    belongs_to :category
    accepts_nested_attributes_for :category
    has_many :comments, :dependent => :delete_all

    def to_param
      slug
    end

    def generate_slug
      self.slug ||= title.parameterize
    end
end
