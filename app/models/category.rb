class Category < ActiveRecord::Base
  validates :title, presence: true, length: { minimum: 2 }
  validates :slug, uniqueness: true, presence: true

  before_validation :generate_slug
  has_many :articles

  def to_param
    slug
  end

  def generate_slug
    self.slug ||= title.parameterize
  end
end
