class CategoriesController < ApplicationController

    http_basic_authenticate_with name: "admin01", password: "humanswilldie"
    before_filter :category, only: [:edit, :update, :destroy]
    before_action :get_category_links, only: [:index, :edit, :update, :new, :create]


    def index
      @categories = Category.all.order('updated_at DESC')
    end

    def new
      @category = Category.new
    end

    def create
      @category = Category.new(params.require(:category).permit(:title, :description))

      if @category.save
        redirect_to action: 'index'
      else
        render 'new'
      end
    end

    def edit
    end

    def update
      if @category.update(params.require(:category).permit(:title, :description))
        redirect_to action: 'index'
      else
        render 'edit'
      end
    end

    def destroy
      @category.destroy
      redirect_to action: 'index'
    end

    private

    def category
      @category = Category.find_by_slug!(params[:id])
    end

    def get_category_links
      @category_links = Category.all.order('created_at ASC')
    end

end
