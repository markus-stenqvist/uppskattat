class ArticlesController < ApplicationController

  http_basic_authenticate_with name: "admin01", password: "humanswilldie", except: [:index, :show, :by_category]
  before_filter :article, only: [:show, :edit, :update, :destroy]
  before_action :get_categories, only: [:new, :create, :edit, :update]
  before_action :get_category_links, only: [:show, :index, :by_category, :edit, :update, :new, :create]

  def index
      @articles = Article.where("publish_time <= ?", Time.now).order('publish_time DESC').page(params[:page]).per_page(5)
  end

  def new
      @article = Article.new
  end

  def create
      _article = params.require(:article).permit(:title, :body, :description, :youtube_id, :image_link, :category_id, :publish_time)
      @article = Article.new(_article)

      if @article.save
          redirect_to @article
      else
          render 'new'
      end
  end

  def show
  end

  def edit
  end

  def by_category
    category = Category.find_by_slug!(params[:id])
    @articles = Article.where(:category_id => category.id).where("publish_time <= ?", Time.now).order('publish_time DESC').page(params[:page]).per_page(5)
  end

  def update
      if @article.update(params.require(:article).permit(:title, :body, :description, :youtube_id, :image_link))
          redirect_to @article
      else
          render 'edit'
      end
  end

  def destroy
    @article.destroy
    redirect_to articles_path
  end

private

  def article
    @article = Article.find_by_slug!(params[:id])
    @category = @article.category
  end

  def get_categories
    @categories = Category.all.map { |category| [category.title, category.id] }
  end

  def get_category_links
    @category_links = Category.all.order('created_at ASC')
  end

end
