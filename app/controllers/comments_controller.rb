class CommentsController < ApplicationController
  def create
    if current_user
      @article = Article.find_by_slug!(params[:article_id])
      @comment = @article.comments.new(comment_params)
      @comment.user = User.find_by_uid(current_user.uid)
      @comment.save
    end

    redirect_to article_path(@article)
  end

  private
  def comment_params
    params.require(:comment).permit(:body)
  end
end
