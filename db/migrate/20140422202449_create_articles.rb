class CreateArticles < ActiveRecord::Migration
  def change

    create_table :categories do |t|
      t.string :title
      t.string :slug, index: true
      t.text :description

      t.timestamps
    end

    create_table :articles do |t|
      t.string :title
      t.string :description
      t.string :youtube_id
      t.string :image_link
      t.text :body
      t.string :slug, index: true
      t.belongs_to :category
      t.datetime :publish_time

      t.timestamps
    end
  end
end
